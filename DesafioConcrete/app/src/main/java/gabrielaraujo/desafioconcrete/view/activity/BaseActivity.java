package gabrielaraujo.desafioconcrete.view.activity;

import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;

import com.arellomobile.mvp.MvpAppCompatActivity;

import gabrielaraujo.desafioconcrete.R;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public class BaseActivity extends MvpAppCompatActivity {

    private Toolbar toolbar;

    public void setUpToolbarText(int resString, boolean isBack){
        setUpToolbarText(getString(resString), isBack);
    }

    public void setUpToolbarText(String title, boolean isBack) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            if (!TextUtils.isEmpty(title)) {
                getSupportActionBar().setDisplayShowTitleEnabled(true);
                getSupportActionBar().setTitle(title);
            } else {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
            if (isBack) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            } else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

package gabrielaraujo.desafioconcrete.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import gabrielaraujo.desafioconcrete.R;
import gabrielaraujo.desafioconcrete.app.Constants;
import gabrielaraujo.desafioconcrete.presenter.detalhe.activity.PullRequestPresenter;
import gabrielaraujo.desafioconcrete.presenter.detalhe.activity.PullRequestPresenterCallback;
import gabrielaraujo.desafioconcrete.service.model.DataDetailPullRequest;
import gabrielaraujo.desafioconcrete.view.adapter.PullRequestAdapter;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public class PullRequestActivity extends BaseActivity implements PullRequestPresenterCallback {

    @BindView(R.id.txt_issue_opend)
    TextView txtIssueOpend;
    @BindView(R.id.list_pull_request)
    RecyclerView listPullRequest;


    @BindView(R.id.txt_no_pull_request)
    TextView txtNoPullRequest;

    @InjectPresenter
    PullRequestPresenter presenter;
    @BindView(R.id.txt_start)
    TextView txtStart;
    @BindView(R.id.txt_fork)
    TextView txtFork;

    @ProvidePresenter
    public PullRequestPresenter provideDetailsPresenter() {
        return new PullRequestPresenter(Parcels.unwrap(getIntent().getParcelableExtra(Constants.EXTRA_DATA_PULL_REQUEST)));
    }

    public static void open(Context context, DataDetailPullRequest dataDetailPullRequest) {
        Intent intent = new Intent(context, PullRequestActivity.class);
        intent.putExtra(Constants.EXTRA_DATA_PULL_REQUEST, Parcels.wrap(dataDetailPullRequest));
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);
        ButterKnife.bind(this);
    }

    @Override
    public void setTitleToolbar(String title) {
        setUpToolbarText(title, true);
    }

    @Override
    public void setIssuesOpen(String issuesOpen) {
        txtIssueOpend.setText(String.format(getString(R.string.issues_opend), issuesOpen));
    }

    @Override
    public void setStart(String star) {
        txtStart.setText(String.format(getString(R.string.start), star));
    }

    @Override
    public void setFork(String qtdeFork) {
        txtFork.setText(String.format(getString(R.string.fork), qtdeFork));
    }

    @Override
    public void setAdapter(PullRequestAdapter adapter) {
        listPullRequest.setLayoutManager(new LinearLayoutManager(this));
        listPullRequest.setAdapter(adapter);
    }

    @Override
    public void showPlaceholder() {
        txtNoPullRequest.setVisibility(View.VISIBLE);
    }
}

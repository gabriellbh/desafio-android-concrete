package gabrielaraujo.desafioconcrete.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gabrielaraujo.desafioconcrete.R;
import gabrielaraujo.desafioconcrete.presenter.detalhe.adapter.PullRequestAdapterPresenter;
import gabrielaraujo.desafioconcrete.presenter.detalhe.adapter.PullRequestAdapterPresenterCallback;
import gabrielaraujo.desafioconcrete.service.model.PullRequestModel;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PullRequestHolder> {

    private List<PullRequestModel> list;
    private Context context;

    public PullRequestAdapter(List<PullRequestModel> list) {
        this.list = list;
    }

    @Override
    public PullRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_detalhe, null);
        return new PullRequestHolder(view, context);
    }

    @Override
    public void onBindViewHolder(PullRequestHolder holder, int position) {
        holder.setData(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class PullRequestHolder extends RecyclerView.ViewHolder implements PullRequestAdapterPresenterCallback {
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_desc)
        TextView txtDesc;
        @BindView(R.id.txt_data_pull_request)
        TextView txtData;
        @BindView(R.id.img_user)
        ImageView imgUser;
        @BindView(R.id.txt_nome)
        TextView txtNome;
        @BindView(R.id.txt_sobrenome)
        TextView txtSobrenome;

        private Context context;

        @InjectPresenter
        PullRequestAdapterPresenter presenter;

        PullRequestHolder(View view, Context context) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);
        }

        public void setData(PullRequestModel pullRequestModel) {
            presenter = new PullRequestAdapterPresenter();
            presenter.attachView(this);
            presenter.setData(pullRequestModel);
        }

        @Override
        public void setTitle(String title) {
            txtTitle.setText(title);
        }

        @Override
        public void setDescricao(String descricao) {
            txtDesc.setText(descricao);
        }

        @Override
        public void setDataPullRequest(String data) {
            txtData.setText(data);
        }

        @Override
        public void setImage(String pathImage) {
            Glide.with(context)
                    .load(pathImage)
                    .placeholder(R.drawable.perfil_load)
                    .bitmapTransform(new CropCircleTransformation(context))
                    .crossFade()
                    .into(imgUser);
        }

        @Override
        public void setNomeUsuario(String nome) {
            txtNome.setText(nome);
        }

        @Override
        public void setCategoria(String categoria) {
            txtSobrenome.setText(categoria);
        }

        @Override
        public void openWeb(String url) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        }

        @OnClick(R.id.container_dad)
        void clickDetalhe(){
            presenter.clickDetalhe();
        }
    }
}

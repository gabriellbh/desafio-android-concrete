package gabrielaraujo.desafioconcrete.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gabrielaraujo.desafioconcrete.R;
import gabrielaraujo.desafioconcrete.presenter.home.activity.HomePresenterCallback;
import gabrielaraujo.desafioconcrete.presenter.home.adapter.HomeAdapterPresenter;
import gabrielaraujo.desafioconcrete.presenter.home.adapter.HomeAdapterPresenterCallback;
import gabrielaraujo.desafioconcrete.service.model.DataDetailPullRequest;
import gabrielaraujo.desafioconcrete.service.model.RepositoryModel;
import gabrielaraujo.desafioconcrete.view.activity.PullRequestActivity;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeHolder> {

    private List<RepositoryModel> list;
    private Context context;
    private HomePresenterCallback homePresenterCallback;

    public HomeAdapter(List<RepositoryModel> list) {
        this.list = list;
    }

    public void setHomePresenterCallback(HomePresenterCallback homePresenterCallback) {
        this.homePresenterCallback = homePresenterCallback;
    }

    @Override
    public HomeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_home, null);
        return new HomeHolder(view, context);
    }

    @Override
    public void onBindViewHolder(HomeHolder holder, int position) {
        holder.setHomePresenterCallback(homePresenterCallback);
        holder.setData(list.get(position));
    }

    public void addItens(List<RepositoryModel> list){
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void clear(){
        if (list != null) list.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HomeHolder extends RecyclerView.ViewHolder implements HomeAdapterPresenterCallback {
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_desc)
        TextView txtDesc;
        @BindView(R.id.txt_fork)
        TextView txtFork;
        @BindView(R.id.txt_star)
        TextView txtStar;
        @BindView(R.id.img_user)
        ImageView imgUser;
        @BindView(R.id.txt_nome)
        TextView txtNome;
        @BindView(R.id.txt_sobrenome)
        TextView txtSobrenome;

        @InjectPresenter
        HomeAdapterPresenter presenter;

        private HomePresenterCallback homePresenterCallback;
        private Context context;

        HomeHolder(View view, Context context) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);
        }

        public void setData(RepositoryModel repositoryModel) {
            presenter = new HomeAdapterPresenter();
            presenter.attachView(this);
            presenter.setData(repositoryModel);
        }

        public void setHomePresenterCallback(HomePresenterCallback homePresenterCallback) {
            this.homePresenterCallback = homePresenterCallback;
        }

        @Override
        public void onLoading(boolean status) {
            homePresenterCallback.onLoading(status);
        }

        @Override
        public void setNomeRepositorio(String nome) {
            txtTitle.setText(nome);
        }

        @Override
        public void setDescricao(String descricao) {
            txtDesc.setText(descricao);
        }

        @Override
        public void setQtdeForks(String forks) {
            txtFork.setText(forks);
        }

        @Override
        public void setQtdeWatchers(String qtde) {
            txtStar.setText(qtde);
        }

        @Override
        public void setImageUser(String pathImage) {
            Glide.with(context)
                    .load(pathImage)
                    .placeholder(R.drawable.perfil_load)
                    .bitmapTransform(new CropCircleTransformation(context))
                    .crossFade()
                    .into(imgUser);
        }

        @Override
        public void setNomeUsuario(String nome) {
            txtNome.setText(nome);
        }

        @Override
        public void setCategoriaUsuario(String categoria) {
            txtSobrenome.setText(categoria);
        }

        @Override
        public void openActivityDetalhe(DataDetailPullRequest dataDetailPullRequest) {
            PullRequestActivity.open(context, dataDetailPullRequest);
        }

        @OnClick(R.id.container_dad)
        void clickDetalhes(){
            presenter.clickDetalhes();
        }
    }
}

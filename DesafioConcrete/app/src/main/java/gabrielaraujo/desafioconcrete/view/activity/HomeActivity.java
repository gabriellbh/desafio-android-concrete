package gabrielaraujo.desafioconcrete.view.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import gabrielaraujo.desafioconcrete.R;
import gabrielaraujo.desafioconcrete.presenter.home.activity.HomePresenter;
import gabrielaraujo.desafioconcrete.presenter.home.activity.HomePresenterCallback;
import gabrielaraujo.desafioconcrete.view.adapter.HomeAdapter;

public class HomeActivity extends BaseActivity implements HomePresenterCallback, OnRefreshListener {

    @BindView(R.id.list_itens)
    RecyclerView listItens;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    @InjectPresenter
    HomePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setUpToolbarText(R.string.title_home, false);
    }

    @Override
    public void init() {
        swipe.setColorSchemeResources(R.color.colorPrimaryDark);
        swipe.setOnRefreshListener(this);
    }

    @Override
    public void setListenerList(RecyclerView.OnScrollListener listener) {
        listItens.addOnScrollListener(listener);
    }

    @Override
    public void onLoading(boolean status) {
        swipe.setRefreshing(status);
    }

    @Override
    public void setAdapter(HomeAdapter adapter) {
        listItens.setLayoutManager(new LinearLayoutManager(this));
        listItens.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        presenter.refreshList();
    }
}

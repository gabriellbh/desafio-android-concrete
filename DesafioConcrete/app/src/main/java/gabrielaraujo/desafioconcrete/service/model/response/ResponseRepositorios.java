package gabrielaraujo.desafioconcrete.service.model.response;

import java.util.List;

import gabrielaraujo.desafioconcrete.service.model.RepositoryModel;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public class ResponseRepositorios {

    private List<RepositoryModel> items;

    public List<RepositoryModel> getItems() {
        return items;
    }

    public void setItems(List<RepositoryModel> items) {
        this.items = items;
    }
}

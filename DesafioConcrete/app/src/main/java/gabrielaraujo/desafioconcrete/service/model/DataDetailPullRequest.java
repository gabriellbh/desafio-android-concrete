package gabrielaraujo.desafioconcrete.service.model;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

@Parcel(Parcel.Serialization.BEAN)
public class DataDetailPullRequest {

    private RepositoryModel repositoryModel;
    private List<PullRequestModel> listPullRequest;

    public DataDetailPullRequest() {
    }

    public DataDetailPullRequest(RepositoryModel repositoryModel, List<PullRequestModel> listPullRequest) {
        super();
        this.repositoryModel = repositoryModel;
        this.listPullRequest = listPullRequest;
    }

    public RepositoryModel getRepositoryModel() {
        return repositoryModel;
    }

    public void setRepositoryModel(RepositoryModel repositoryModel) {
        this.repositoryModel = repositoryModel;
    }

    public List<PullRequestModel> getListPullRequest() {
        return listPullRequest;
    }

    public void setListPullRequest(List<PullRequestModel> listPullRequest) {
        this.listPullRequest = listPullRequest;
    }
}

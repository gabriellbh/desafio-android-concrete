package gabrielaraujo.desafioconcrete.service.retrofit;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

import gabrielaraujo.desafioconcrete.app.Constants;
import gabrielaraujo.desafioconcrete.service.model.PullRequestModel;
import gabrielaraujo.desafioconcrete.service.model.response.ResponseRepositorios;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by danielnazareth on 28/12/16.
 */

public interface RestApi {

    @GET(Constants.Service.GET_REPOSITORIOS)
    Call<ResponseRepositorios> getRepositorios(@Query("page") int page);

    @GET(Constants.Service.GET_PULL_REQUEST)
    Call<List<PullRequestModel>> getPullRequest(@Path("criador") String criador, @Path("repositorio") String repositorio);
}

package gabrielaraujo.desafioconcrete.app;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public class Constants {

    public static final int MAX_PER_PAGE = 30;
    public static final String EXTRA_DATA_PULL_REQUEST = "extra_data_pull_request";

    public static class Service{
        public static final String GET_REPOSITORIOS = "search/repositories?q=language:Java&sort=stars";
        public static final String GET_PULL_REQUEST = "repos/{criador}/{repositorio}/pulls";
    }
}

package gabrielaraujo.desafioconcrete.presenter.home.adapter;

import com.arellomobile.mvp.MvpView;

import gabrielaraujo.desafioconcrete.service.model.DataDetailPullRequest;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public interface HomeAdapterPresenterCallback extends MvpView {

    void onLoading(boolean status);

    void setNomeRepositorio(String nome);
    void setDescricao(String descricao);
    void setQtdeForks(String forks);
    void setQtdeWatchers(String qtde);
    void setImageUser(String pathImage);
    void setNomeUsuario(String nome);
    void setCategoriaUsuario(String categoria);

    void openActivityDetalhe(DataDetailPullRequest dataDetailPullRequest);
}

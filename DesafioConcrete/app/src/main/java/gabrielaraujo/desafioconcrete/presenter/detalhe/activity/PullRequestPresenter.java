package gabrielaraujo.desafioconcrete.presenter.detalhe.activity;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import gabrielaraujo.desafioconcrete.service.model.DataDetailPullRequest;
import gabrielaraujo.desafioconcrete.utils.Utils;
import gabrielaraujo.desafioconcrete.view.adapter.PullRequestAdapter;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

@InjectViewState
public class PullRequestPresenter extends MvpPresenter<PullRequestPresenterCallback> {

    private DataDetailPullRequest dataDetailPullRequest;
    private PullRequestAdapter adapter;

    public PullRequestPresenter(DataDetailPullRequest dataDetailPullRequest) {
        this.dataDetailPullRequest = dataDetailPullRequest;

        setData();
    }

    private void setData(){
        getViewState().setTitleToolbar(dataDetailPullRequest.getRepositoryModel().getName());

        getViewState().setIssuesOpen(String.valueOf(Utils.getNumFormat(dataDetailPullRequest.getRepositoryModel().getOpen_issues())));
        getViewState().setStart(String.valueOf(Utils.getNumFormat(dataDetailPullRequest.getRepositoryModel().getStargazers_count())));
        getViewState().setFork(String.valueOf(Utils.getNumFormat(dataDetailPullRequest.getRepositoryModel().getForks())));

        initAdapter();
    }

    private void initAdapter(){
        if (dataDetailPullRequest.getListPullRequest().size() > 0){
            adapter = new PullRequestAdapter(dataDetailPullRequest.getListPullRequest());
            getViewState().setAdapter(adapter);
        }else{
            getViewState().showPlaceholder();
        }
    }
}

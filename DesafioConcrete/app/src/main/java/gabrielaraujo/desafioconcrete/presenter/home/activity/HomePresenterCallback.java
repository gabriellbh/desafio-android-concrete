package gabrielaraujo.desafioconcrete.presenter.home.activity;

import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.MvpView;

import gabrielaraujo.desafioconcrete.view.adapter.HomeAdapter;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public interface HomePresenterCallback extends MvpView {

    void init();
    void setListenerList(RecyclerView.OnScrollListener listener);

    void onLoading(boolean status);

    void setAdapter(HomeAdapter adapter);
}

package gabrielaraujo.desafioconcrete.presenter.detalhe.adapter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import gabrielaraujo.desafioconcrete.service.model.PullRequestModel;
import gabrielaraujo.desafioconcrete.utils.Utils;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

@InjectViewState
public class PullRequestAdapterPresenter extends MvpPresenter<PullRequestAdapterPresenterCallback> {

    private PullRequestModel pullRequestModel;

    public PullRequestAdapterPresenter() {
    }

    public void setData(PullRequestModel pullRequestModel) {
        this.pullRequestModel = pullRequestModel;

        getViewState().setTitle(pullRequestModel.getTitle());
        getViewState().setDescricao(pullRequestModel.getBody());
        getViewState().setDataPullRequest(Utils.getDateAdapterSolicitacao(pullRequestModel.getCreated_at()));

        getViewState().setNomeUsuario(pullRequestModel.getUser().getLogin());
        getViewState().setImage(pullRequestModel.getUser().getAvatar_url());
        getViewState().setCategoria(pullRequestModel.getUser().getType());
    }

    public void clickDetalhe(){
        getViewState().openWeb(pullRequestModel.getHtml_url());
    }
}

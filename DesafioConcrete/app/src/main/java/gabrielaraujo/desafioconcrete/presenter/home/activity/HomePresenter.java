package gabrielaraujo.desafioconcrete.presenter.home.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import gabrielaraujo.desafioconcrete.service.model.RepositoryModel;
import gabrielaraujo.desafioconcrete.service.model.response.ResponseRepositorios;
import gabrielaraujo.desafioconcrete.service.retrofit.RetrofitBase;
import gabrielaraujo.desafioconcrete.view.adapter.HomeAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

@InjectViewState
public class HomePresenter extends MvpPresenter<HomePresenterCallback> {

    private HomeAdapter adapter;
    private int page = 1;
    private boolean isMore = false;

    public HomePresenter() {
        getViewState().init();
        getViewState().setListenerList(getScrollListener());
        getRepositorios();
    }

    public void getRepositorios() {
        getViewState().onLoading(true);
        RetrofitBase.getInterfaceRetrofit().getRepositorios(page)
                .enqueue(new Callback<ResponseRepositorios>() {
                    @Override
                    public void onResponse(Call<ResponseRepositorios> call, Response<ResponseRepositorios> response) {
                        getViewState().onLoading(false);
                        if (response.isSuccessful()) {
                            List<RepositoryModel> listRepositorios = response.body().getItems();
                            if (listRepositorios.size() == 30){
                                isMore = true;
                                page++;
                            }else{
                                isMore = false;
                            }
                            initAdapter(listRepositorios);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseRepositorios> call, Throwable t) {
                        getViewState().onLoading(false);
                    }
                });
    }

    private void initAdapter(List<RepositoryModel> list) {
        if (list.size() > 0) {
            if (adapter == null) {
                adapter = new HomeAdapter(list);
                adapter.setHomePresenterCallback(getViewState());
                getViewState().setAdapter(adapter);
            } else {
                adapter.addItens(list);
            }
        }
    }

    public void refreshList(){
        page = 1;
        isMore = false;
        if (adapter != null) adapter.clear();
        getRepositorios();
    }

    public RecyclerView.OnScrollListener getScrollListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    int pastVisibleItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                    if (((visibleItemCount + pastVisibleItems) >= totalItemCount)) {
                        if (isMore) {
                            isMore = false;
                            getRepositorios();
                        }
                    }
                }
            }
        };
    }
}

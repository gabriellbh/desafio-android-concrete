package gabrielaraujo.desafioconcrete.presenter.detalhe.adapter;

import com.arellomobile.mvp.MvpView;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public interface PullRequestAdapterPresenterCallback extends MvpView {


    void setTitle(String title);
    void setDescricao(String descricao);
    void setDataPullRequest(String data);
    void setImage(String pathImage);
    void setNomeUsuario(String nome);
    void setCategoria(String categoria);

    void openWeb(String url);
}

package gabrielaraujo.desafioconcrete.presenter.detalhe.activity;

import com.arellomobile.mvp.MvpView;

import gabrielaraujo.desafioconcrete.view.adapter.PullRequestAdapter;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public interface PullRequestPresenterCallback extends MvpView {

    void setTitleToolbar(String title);

    void setIssuesOpen(String issuesOpen);
    void setStart(String star);
    void setFork(String qtdeFork);

    void setAdapter(PullRequestAdapter adapter);

    void showPlaceholder();
}

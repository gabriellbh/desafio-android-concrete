package gabrielaraujo.desafioconcrete.presenter.home.adapter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import gabrielaraujo.desafioconcrete.service.model.DataDetailPullRequest;
import gabrielaraujo.desafioconcrete.service.model.PullRequestModel;
import gabrielaraujo.desafioconcrete.service.model.RepositoryModel;
import gabrielaraujo.desafioconcrete.service.retrofit.RetrofitBase;
import gabrielaraujo.desafioconcrete.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

@InjectViewState
public class HomeAdapterPresenter extends MvpPresenter<HomeAdapterPresenterCallback> {

    private RepositoryModel repositoryModel;

    public HomeAdapterPresenter() {
    }

    public void setData(RepositoryModel repositoryModel) {
        this.repositoryModel = repositoryModel;

        getViewState().setNomeRepositorio(repositoryModel.getName());
        getViewState().setDescricao(repositoryModel.getDescription());
        getViewState().setQtdeForks(String.valueOf(Utils.getNumFormat(repositoryModel.getForks())));
        getViewState().setQtdeWatchers(String.valueOf(Utils.getNumFormat(repositoryModel.getStargazers_count())));

        getViewState().setImageUser(repositoryModel.getOwner().getAvatar_url());
        getViewState().setNomeUsuario(repositoryModel.getOwner().getLogin());
        getViewState().setCategoriaUsuario(repositoryModel.getOwner().getType());
    }

    public void clickDetalhes() {
        getViewState().onLoading(true);
        RetrofitBase.getInterfaceRetrofit().getPullRequest(repositoryModel.getOwner().getLogin(), repositoryModel.getName())
                .enqueue(new Callback<List<PullRequestModel>>() {
                    @Override
                    public void onResponse(Call<List<PullRequestModel>> call, Response<List<PullRequestModel>> response) {
                        getViewState().onLoading(false);
                        if (response.isSuccessful()) {
                            DataDetailPullRequest data = new DataDetailPullRequest(repositoryModel, response.body());
                            getViewState().openActivityDetalhe(data);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<PullRequestModel>> call, Throwable t) {
                        getViewState().onLoading(false);
                    }
                });
    }
}

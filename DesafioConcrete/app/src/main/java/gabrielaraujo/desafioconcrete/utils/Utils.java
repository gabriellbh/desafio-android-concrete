package gabrielaraujo.desafioconcrete.utils;

import android.text.TextUtils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by gabrielaraujo on 05/01/17.
 */

public class Utils {

    public static String getNumFormat(double value) {
        if (value == 0) {
            return "-";
        }
        NumberFormat numberFormat = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
        return numberFormat.format(value);
    }

    public static String getDateAdapterSolicitacao(String startDate) {
        if (!TextUtils.isEmpty(startDate)) {
            SimpleDateFormat simpleDateFormatString = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy 'às' HH:mm");
            try {
                Date date = simpleDateFormatString.parse(startDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                return simpleDateFormat.format(calendar.getTime());
            } catch (ParseException e) {
                return "";
            }
        } else {
            return "";
        }
    }
}
